resource "aws_sfn_state_machine" "sfn_state_machine" {
  name     = "iar"
  role_arn = "arn:aws:iam::398385920689:role/stepfunction_role"

  definition = <<EOF
{
  "Comment": "Work flow  for triggering datasync",
  "StartAt": "Invoke datasync to transfer file to S3",
  "States": {
    "Invoke datasync to transfer file to S3": {
      "Type": "Task",
      "Resource": "arn:aws:states:::lambda:invoke",
      "Parameters": {
        "FunctionName": "arn:aws:lambda:eu-west-1:295129861662:function:lmb-bi-np-euwe01-datasync-01:$LATEST",
        "Payload": {
          "Input": {
            "task_arn": "arn:aws:datasync:eu-west-1:295129861662:task/task-02a761cfcb48ea691"
          }
        }
      },
      "TimeoutSeconds": 10800,
      "Next": "wait_ten_seconds",
      "Catch": [
        {
          "ErrorEquals": [
            "States.ALL"
          ],
          "Next": "Notify Failure"
        }
      ]
    },
    "wait_ten_seconds": {
      "Type": "Wait",
      "Seconds": 300,
      "Next": "Invoke datasync agent reverse"
    },
    "Invoke datasync agent reverse": {
      "Type": "Task",
      "Resource": "arn:aws:states:::lambda:invoke",
      "Parameters": {
        "FunctionName": "arn:aws:lambda:eu-west-1:295129861662:function:lmb-bi-np-euwe01-datasync-01:$LATEST",
        "Payload": {
          "Input": {
            "task_arn": "arn:aws:datasync:eu-west-1:295129861662:task/task-0d7917f308deb6c20"
          }
        }
      },
      "Next": "Succeed State",
      "Catch": [
        {
          "ErrorEquals": [
            "States.ALL"
          ],
          "Next": "Notify Failure"
        }
      ]
    },
    "Succeed State": {
      "Type": "Succeed"
    },
    "Notify Failure": {
      "Type": "Task",
      "Resource": "arn:aws:states:::sns:publish",
      "Parameters": {
        "Subject": "stf-bi-np-euwe01-cds-datasync-01 - Failed",
        "Message": "stf-bi-np-euwe01-cds-datasync-01 - datasync job submitted through Step Function stf-bi-np-euwe01-cds-datasync-01 failed. -Source : Step Function -Severity : Major -Action to be Performed : Create Incident in SNOW -Assign : NA -ApplicationAcronym : BI -AWS Account : 295129861662",
        "TopicArn": "arn:aws:sns:eu-west-1:295129861662:sns-bi-np-euwe01-cds-datasync-01"
      },
      "Next": "Failed"
    },
    "Failed": {
      "Type": "Fail",
      "Error": "ErrorCode",
      "Cause": "Caused By Message"
    }
  }
}
EOF
}
