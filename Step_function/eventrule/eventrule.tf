resource "aws_cloudwatch_event_rule" "console" {
  name = "cwr-bi-euwe01-cds-datasync-01"
  description = "Scheduler is to hit the Step function"
  schedule_expression = "rate(1 minute)"
  is_enabled = "false"
}

resource "aws_cloudwatch_event_target" "step" {
  rule      = aws_cloudwatch_event_rule.console.id
  target_id = "Stepfunction"
  arn       = aws_sfn_state_machine.step_fn.arn
  role_arn = "arn:aws:iam::398385920689:role/iar-stepfunction-execute"
}

resource "aws_sfn_state_machine" "step_fn" {
  name = "stf-bi-ac-euwe01-cds-datasync-01"
  role_arn = "arn:aws:iam::398385920689:role/StepFunctions-stf-bi-np-euwe01-cds-datasync-01-role-c0ada18f"
  definition = file("./definition.json")
}
