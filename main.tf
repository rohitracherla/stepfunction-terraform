
module "step_functions" {
  source = "./Newfolder"
}


 module "event-rule" {
      source              = "./Newfolder1"
      #target_id = "stepfunction"
    }
	
	
resource "aws_s3_bucket" "bucket" {
  bucket = "testinglifecycle123456"
  acl    = "private"

  lifecycle_rule {
    id      = "myLifecycle"
    enabled = true

    #prefix = "log/"

    tags = {
      rule      = "log"
      autoclean = "true"
    }

    expiration {
      days = 1
    }
  }
  }