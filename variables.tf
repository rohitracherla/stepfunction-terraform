variable "Default_Tags" {
  default = ""
}
variable "App_EnvironmentType" {
  default = ""
}
variable "App_ClusterCode" {
  default = ""
}
variable "App_ApplicationName" {
  default = ""
}
variable "AWS_Account_Number" {
  default = ""
}