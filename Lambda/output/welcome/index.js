let AWS=require('aws-sdk');
let datasync= new AWS.DataSync({apiVersion: '2018-11-09'});
var taskArn="";
var taskExecutionArn="";
//Start execute the datasync task
function startTaskExecution(callback){
    var startTaskParams = {
        TaskArn: taskArn
    };

    console.log("Invoke task "+taskArn);
    datasync.startTaskExecution(startTaskParams, function(err, data) {
        if (err) {
            console.log(err, err.stack);
            console.log("ERROR: at datasync task check datasync task for more logs");
            callback(err);

        }
        else  {
            console.log(data);
            taskExecutionArn=data.TaskExecutionArn;
            checkTaskStatus(callback);
        }
    });
}
//To check status of datasync execution
function checkTaskStatus(callback){
    var params = {
        TaskArn: taskArn
    };
    var status="";
    console.log("checkTaskStatus task");
    datasync.describeTask(params, function(err, data) {
        if (err) {
            console.log(err, err.stack);
            callback(err);
        }
        else  {
            status=data.Status;
            console.log("Status "+data.Status);
            if(status != "QUEUED"){
                if(taskExecutionArn==data.CurrentTaskExecutionArn){
                    console.log("INSIDE Task execution arn check....................");
                    checkTaskexecutionStatus();
                }
            }
            else{
                checkTaskStatus();
                console.log("INSIDE status check....................");

            }
        }
    });
}

function checkTaskexecutionStatus(callback){

    var params = {
        TaskExecutionArn: taskExecutionArn
    };
    datasync.describeTaskExecution(params, function(err, data) {
        if (err) {
            console.log(err, err.stack);
            callback(err);
        }
        else{
            var executionstatus ="";
            executionstatus=data.Status;

            if(executionstatus=="ERROR"){
                console.log("ERROR: at datasync task check datasync task for more logs");
                console.log("Status inside error"+executionstatus);
                throw (executionstatus);
            }else if(executionstatus !="SUCCESS"){
                checkTaskexecutionStatus();
                console.log("INSIDE status check....................");
                console.log("Status inside success"+executionstatus);
            }
        }
    });

}
let handler = function (event, context, callback) {
    taskArn=event.Input.task_arn;
    try {
        startTaskExecution(callback);
    } catch (error) {
        console.log(error);
        callback(error);
    }

}
module.exports.handler = handler